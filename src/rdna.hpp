#include <string>
#include <iostream>
#include <random>

using std::string;
using namespace std;

string randDNA(int seed, string bases, int n)
{
string kappa;
int min = 0;
int max = bases.size();

mt19937 engine(seed);
uniform_int_distribution<>unifrm(min,max-1);

for (int i=0;i<n;i++)
{
	
	kappa += bases[unifrm(engine)];
}

return kappa;
}
